﻿using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;


namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly SetPartnerPromoCodeLimitRequest limitSettings;
        private readonly SetPartnerPromoCodeLimitRequest wrongLimitSettings;

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();

            limitSettings = new SetPartnerPromoCodeLimitRequest
            {
                EndDate = DateTime.Now.AddDays(3),
                Limit = 100
            };

            wrongLimitSettings = new SetPartnerPromoCodeLimitRequest
            {
                EndDate = DateTime.Now.AddDays(-3),
                Limit = -100
            };
        }

        public Partner CreateTestPartner(bool isActive, bool withCancelDate = false, 
            int numberIssuedPromoCodes = 20)
        {
            var limit = new PartnerPromoCodeLimit
            {
                Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                CreateDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(30),
                Limit = 100
            };

            if (withCancelDate == true)
            {
                limit.CancelDate = DateTime.Now;
            }

            var partner = new Partner
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Спортмастер",
                IsActive = isActive,
                NumberIssuedPromoCodes = numberIssuedPromoCodes,
                PartnerLimits = new List<PartnerPromoCodeLimit>() { limit }
            };

            return partner;
        }

        /// <summary>
        /// Если партнер не найден, то также нужно выдать ошибку 404;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            Partner partner = null;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitSettings);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }

        /// <summary>
        /// Если партнер заблокирован, то есть поле IsActive = false в классе Partner, то также нужно выдать ошибку 400;
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateTestPartner(false);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitSettings);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, которые партнер выдал NumberIssuedPromoCodes
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimitToPartnerWithActiveLimit_NumberIssuedPromoCodesSetToZero()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateTestPartner(true);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);
            
            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitSettings);
            
            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(0);
        }

        /// <summary>
        /// Если партнеру выставляется новый лимит и если текущий лимит закончился, то количество промокодов,
        /// которые партнер выдал NumberIssuedPromoCodes, не обнуляется
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimitToPartnerWithNotActiveLimit_NumberIssuedPromoCodesDoesNotChange()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateTestPartner(true, true);

            // Текущее значение выданных промокодов
            var initialNumberIssuedPromoCodes = partner.NumberIssuedPromoCodes;

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitSettings);

            // Assert
            partner.NumberIssuedPromoCodes.Should().Be(initialNumberIssuedPromoCodes);
        }

        /// <summary>
        /// При установке лимита нужно отключить предыдущий лимит
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimitToPartnerWithActiveLimit_CancelDateIsExists()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateTestPartner(true);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitSettings);

            // Assert
            partner.PartnerLimits.FirstOrDefault(i => i.CancelDate != null).Should().NotBeNull();
        }

        /// <summary>
        /// Лимит должен быть больше 0 (тест на попытку установить некорректный лимит)
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetWrongLimitToPartnerWithActiveLimit_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateTestPartner(true);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, wrongLimitSettings);

            // Assert
            result.Should().BeAssignableTo<BadRequestObjectResult>();
        }

        /// <summary>
        /// Нужно убедиться, что сохранили новый лимит в базу данных
        /// </summary>
        [Fact]
        public async void SetPartnerPromoCodeLimitAsync_SetNewLimitToPartnerWithActiveLimit_LimitSaved()
        {
            // Arrange
            var partnerId = Guid.Parse("def47943-7aaf-44a1-ae21-05aa4948b165");
            var partner = CreateTestPartner(true);

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partnerId))
                .ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, limitSettings);

            // Assert
            partner.PartnerLimits.FirstOrDefault(o => o.CancelDate == null).Should().NotBeNull();
            _partnersRepositoryMock.Verify(o => o.UpdateAsync(It.IsAny<Partner>()), Times.Once);
        }
    }
}